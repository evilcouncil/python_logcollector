import requests
import json


class LogCollector:
    """Collect logs from a configured endpoint with given client certs."""

    def __init__(
        self,
        server: str,
        port: int,
        client_cert: str,
        client_key: str,
        ca_cert: str,
    ):
        self._url = f"https://{server}:{port}"
        self._client_cert = client_cert
        self._client_key = client_key
        self._ca_cert = ca_cert

    def list_files(self) -> dict:
        data = requests.get(
            f"{self._url}/v1/files",
            cert=(self._client_cert, self._client_key),
            verify=self._ca_cert,
        )

        return json.loads(data.text)

    def get_file(self, target) -> str:
        data = requests.get(
            f"{self._url}/v1/files/{target}",
            cert=(self._client_cert, self._client_key),
            verify=self._ca_cert,
        )

        return data.text

    def delete_file(self, target) -> str:
        data = requests.delete(
            f"{self._url}/v1/files/{target}",
            cert=(self._client_cert, self._client_key),
            verify=self._ca_cert,
        )

        return data.text
